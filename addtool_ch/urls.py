"""addtool_ch URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url

from django.urls import include, path

from django.contrib import admin
from addtool import views

urlpatterns = [
    path('filter/', views.filter_possible, name="filter"),
    path('export/', views.export_csv, name="export"),
    path('accounts/', include('django.contrib.auth.urls')),
    path('update/<int:pk>/', views.AddressUpdate.as_view(), name="update"),
    path('delete/<int:pk>/', views.delete, name="delete"),
    path('import/', views.import_list, name="import"),
    path('list/', views.address_list, name="input"),
    path('getlist/', views.get_list, name="get-list"),
    path('admin/', admin.site.urls),
    path('', views.index, name="index"),
]
