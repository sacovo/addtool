from django.apps import AppConfig


class AddtoolConfig(AppConfig):
    name = 'addtool'
