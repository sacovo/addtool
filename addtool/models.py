from django.db import models

# Create your models here.
class Building(models.Model):
    plz = models.CharField(max_length=5, verbose_name="PLZ")
    place = models.CharField(max_length=100, verbose_name="Ort")
    street = models.CharField(max_length=100, verbose_name="Strasse")
    nr = models.CharField(max_length=5, verbose_name="Nummer", blank=True)

class AddressList(models.Model):
    slug = models.SlugField(primary_key=True)

class Address(models.Model):
    plz = models.CharField(max_length=5, verbose_name="PLZ")
    place = models.CharField(max_length=100, verbose_name="Ort")
    street = models.CharField(max_length=100, verbose_name="Strasse")
    nr = models.CharField(max_length=5, verbose_name="Nummer", blank=True)
    first_name = models.CharField(max_length=100, verbose_name="Vorname", blank=True)
    last_name = models.CharField(max_length=100, verbose_name="Nachname", blank=True)
    anrede = models.CharField(max_length=100, verbose_name="Anrede", blank=True)
    birthday = models.CharField(max_length=15, verbose_name="Geburtsdatum", blank=True)
    l = models.ForeignKey(AddressList, models.CASCADE)
    invalid = models.CharField(max_length=100, verbose_name="Ungültig", blank=True)
    
    def get_absolute_url(self):
        return '/list/?slug=' + self.l.slug
