import csv
import string
import random
import tempfile

from django.contrib import messages
from django.shortcuts import render, get_object_or_404, redirect
from django.views.generic.edit import UpdateView
from django.http import HttpResponse

from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin


from django_ajax.decorators import ajax

from addtool.models import AddressList, Building, Address
from addtool.forms import ImportForm

 

# Create your views here.
def random_string(size=30, chars=string.ascii_uppercase + string.digits):
    s = ''
    while True:
        s = ''.join(random.choice(chars) for x in range(size))
        if not AddressList.objects.filter(slug=s).exists():
            return s


@login_required
def index(request):
    return render(request, 'addtool/index.html', {
        'slug': random_string()
    })


@login_required
def address_list(request):
    slug = request.GET['slug']
    if AddressList.objects.filter(slug=slug).exists():
        alist = AddressList.objects.get(slug=slug)
    else:
        alist = AddressList.objects.create(slug=slug)
    if request.POST:
        if all([request.POST['plz'], request.POST['place'],
                request.POST['first_name'], request.POST['street'],
                request.POST['last_name'],]):
            a = Address.objects.create(
                plz=request.POST['plz'],
                place=request.POST['place'],
                street=request.POST['street'],
                nr=request.POST['nr'],
                first_name=request.POST['first_name'],
                last_name=request.POST['last_name'],
                anrede=request.POST['anrede'],
                birthday=request.POST['birthday'],
                l=alist,
                invalid=request.POST['invalid']
            )
            # PLZ Correction
            if Building.objects.filter(place__startswith=a.place, street=a.street).exists():
                if not Building.objects.filter(place__startswith=a.place, plz=a.plz, street=a.street).exists():
                    a.plz = Building.objects.filter(place__startswith=a.place, street=a.street)[0].plz
                    a.save()

        return redirect('/list/?slug=' + alist.slug + "&plz="+request.POST['plz'] + "&place="+request.POST['place'])

    return render(request, 'addtool/add_input.html', context={
        'list': alist,
        'last_names': set(Address.objects.all().values_list('last_name', flat=True)),
        'first_names': set(Address.objects.all().values_list('first_name', flat=True)),
    })

def handle_import_file(csvfile):
    with tempfile.NamedTemporaryFile('w+b', suffix='.csv') as import_file:
        for chunk in csvfile.chunks():
            import_file.write(chunk)
        import_file.flush()
        with open(import_file.name) as csvfile:
            reader = csv.reader(csvfile, delimiter=';')
            for row in reader:
                Building.objects.create(
                    plz=row[7], 
                    place=row[9],
                    street=row[5],
                    nr=row[6])

@login_required
def import_list(request):
    form = ImportForm
    if request.method == 'POST':
        form = ImportForm(request.POST, request.FILES)
        if form.is_valid():
            result = handle_import_file(request.FILES['csv_file'])
            return redirect('index')
    return render(request, 'addtool/import.html', {
        'form': form
    })

@login_required
@ajax
def filter_possible(request):
    field = request.GET['field']
    plz_filter = request.GET.get('plz', '')
    place_filter = request.GET.get('place', '')
    if field=='place':
        place_filter = ''
    street_filter = request.GET.get('street', '')
    nr_filter = request.GET.get('nr', '')
    query = Building.objects.all()
    if plz_filter and (not place_filter) and field!='plz':
        query = query.filter(plz=plz_filter)
    if place_filter and field!='place':
        query = query.filter(place__startswith=place_filter)
    if street_filter and field!='street':
        query = query.filter(street=street_filter)
    if nr_filter and field!='nr':
        query = query.filter(nr=nr_filter)
    fields = set(query.values_list(field, flat=True))
    return {'result': fields, 'field': field}

@login_required
@ajax
def get_list(request):
    print(request)
    slug = request.GET['slug']
    al = get_object_or_404(AddressList, slug=slug).address_set.all()
    print(al)
    return {'result': al}

class AddressUpdate(LoginRequiredMixin, UpdateView):
    model = Address
    fields = ['plz', 'place', 'street', 'nr', 'first_name', 'last_name', 'anrede', 'birthday', 'invalid']
    template_name_suffix = '_update_form'

@login_required
def delete(request, pk):
    if request.POST:
        slug = request.POST['slug']
        address = get_object_or_404(Address, pk=pk)
        if slug == address.l.slug:
            address.delete()
            return redirect('/list/?slug='+slug)
    return redirect('/update/'+str(pk)+'/')


def export_csv(request):
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="export.csv"'

    writer = csv.writer(response)

    slugs = request.GET['slugs'].split(',')
    addresses = Address.objects.filter(l__in=slugs)
    writer.writerow(['Vorname', 'Name', 'Anrede', 'Strasse', 'Nummer', 'PLZ', 'Ort', 'Geburtstag', 'Ungültig'])
    for a in addresses:
        writer.writerow([a.first_name, a.last_name, a.anrede, a.street, a.nr, a.plz, a.place, a.birthday, a.invalid])
    return response


